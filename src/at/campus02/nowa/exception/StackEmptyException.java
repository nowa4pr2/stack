package at.campus02.nowa.exception;

public class StackEmptyException extends StackException {

	private static final long serialVersionUID = 2521728674953728212L;

	public StackEmptyException() {}
	
	public StackEmptyException(String msg) {
		super(msg);
	}
	
}
