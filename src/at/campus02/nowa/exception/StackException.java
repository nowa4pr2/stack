package at.campus02.nowa.exception;

public class StackException extends Exception {

	private static final long serialVersionUID = -5690530409490842454L;

	public StackException() {
	}

	public StackException(String msg) {
		super(msg);
	}
}
