package at.campus02.nowa.exception;

public class StackFullExeption extends StackException {
	

	private static final long serialVersionUID = -9067811714416599676L;

	public StackFullExeption() {
	}

	public StackFullExeption(String msg) {
		super(msg);
	}
	
	public StackFullExeption(int msg) {
		super(String.valueOf(msg));
	}
}
