package at.campus02.nowa.model;

import at.campus02.nowa.exception.StackEmptyException;
import at.campus02.nowa.exception.StackFullExeption;

public class Stack {
	private int index = 0;
	private Note[] notes ;
	private int pointer;
	
	public Stack(int size) {
		notes = new Note[size];
	}
	
	public Note[] getNotes() {
		return notes;
	}

	public void setNotes(Note[] notes) {
		this.notes = notes;
	}

	public int getPointer() {
		return pointer;
	}

	public void setPointer(int pointer) {
		this.pointer = pointer;
	}
	/**
	 * 
	 * @param note - Note object
	 * @throws StackFullExeption - Stack is full.
	 */
	public void push(Note note) throws StackFullExeption {
		if(index == notes.length -1) {
			throw new StackFullExeption(notes.length);
		}
		
		notes[++index] = note;
	}
	/**
	 * 
	 * @return Note Object
	 * @throws StackEmptyException - Stack is empty
	 */
	public Note pop() throws StackEmptyException {
		if(index == 0) {
			throw new StackEmptyException("cannot pop stack");
		}
		Note n = notes[index];
		notes[index--] = null;
		return n;
	}
}
