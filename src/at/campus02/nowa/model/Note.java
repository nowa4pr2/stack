package at.campus02.nowa.model;

import java.time.LocalDate;
import java.util.Date;

public class Note {

	private LocalDate date;
	private String title;
	private String description;
	
	
	public Note(LocalDate date, String title, String description) {
		this.date = date;
		this.title = title;
		this.description = description;
	}


	public LocalDate getDate() {
		return date;
	}


	public void setDate(LocalDate date) {
		this.date = date;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String toString() {
		return "Note [date=" + date + ", title=" + title + ", description=" + description + "]";
	}
	
}
