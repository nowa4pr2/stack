package at.campus02.nowa;

import java.time.LocalDate;

import at.campus02.nowa.exception.StackEmptyException;
import at.campus02.nowa.exception.StackException;
import at.campus02.nowa.exception.StackFullExeption;
import at.campus02.nowa.model.Note;
import at.campus02.nowa.model.Stack;

public class StackDemo {

	public static void main(String[] args) {
		int numNotes = 10;
		
		// Stack instanziieren
		Stack stack ;//= new Stack(numNotes);
		
		try {
//			stack.push(new Note (LocalDate.now(), "TODO", "DESC"));
//			System.out.println(stack.pop());
//			for(int i = 0; i > numNotes; i++) {
//				stack.push(new Note(LocalDate.now(), "TODO" + (i+1), "DESC" + (i+1)));
//			}
//			System.out.println(stack.pop());
//			System.out.println(stack.pop());
			
//			 Bsp 1
//			int i = 0;
//			while (i <= 10) {
//				stack.push(new Note(LocalDate.now(), "TODO" + (i+1), "DESC" + (i+1)));
//			}
//			stack.pop();
//			
			//Bsp 2
//			stack.pop();
//			while(true) {
//				stack.push(new Note(LocalDate.now(), "TODO", "DESC"));
//			}
			
			//Bsp 3
//			stack.pop();
//			stack.push(new Note(LocalDate.now(), "TODO", "DESC"));
			
			//Bsp 4
			numNotes = 2;
		    stack = new Stack(numNotes);
			stack.push(new Note(LocalDate.now(), "TODO", "DESC"));
			stack.push(new Note(LocalDate.now(), "TODO", "DESC"));
			stack.push(new Note(LocalDate.now(), "TODO", "DESC"));
			stack.pop();
			
			//Bsp 5
		} catch( StackFullExeption e ) {
//			e.printStackTrace();
			System.out.println("Stack full");
		} catch(StackException e ) {
//			e.printStackTrace();
			System.out.println("other stack failure");
		}
		
	}
}
